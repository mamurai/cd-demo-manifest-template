#!/bin/bash

if [ "$(oc whoami)" != "system:admin" -a "$(oc whoami)" != "opentlc-mgr" ]; then
  echo "Login as admin or opentlc-mgr first."
  exit
fi

echo -n "Input GITLAB_USER (current: ${GITLAB_USER}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_USER=${REPLY}
fi

echo -n "Input GITLAB_USER_EMAIL (current: ${GITLAB_USER_EMAIL}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_USER_EMAIL=${REPLY}
fi

echo -n "Input GITLAB_TOKEN (current: ${GITLAB_TOKEN}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_TOKEN=${REPLY}
fi

echo GITLAB_USER:       ${GITLAB_USER}
echo GITLAB_USER_EMAIL: ${GITLAB_USER_EMAIL}
echo GITLAB_TOKEN:      ${GITLAB_TOKEN}

echo -n "Input variable is correct?  (yes or no) "
read
if [ "${REPLY}" != "yes" ]; then
  exit
fi



# Operator install
echo "### Pipeline & Gitops Operator install ###"
cd ${HOME}/cd-demo-manifest

# OpenShift Pipeline operator Install
echo "### OpenShift Pipeline operator Install ###"
oc apply -f ./handson/pipelines/prep/openshift-pipeline-install.yaml
sleep 10;

# OpenShift GitOps operator Install
echo "### OpenShift GitOps operator Install ###"
oc apply -f ./handson/pipelines/prep/openshift-gitops-install.yaml
sleep 20;

# Create your projects
echo "### Create your projects ###"
echo "### Create {GITLAB_USER}-develop project ###"
oc new-project ${GITLAB_USER}-develop
sleep 10;

echo "### Create {GITLAB_USER}-staging project ###"
oc new-project ${GITLAB_USER}-staging
sleep 10;

echo "### Create {GITLAB_USER}-production project ###"
oc new-project ${GITLAB_USER}-production
sleep 10;


# SonerQube deploy
echo "### SonerQube deploy on ${GITLAB_USER}-develop ###"
oc project ${GITLAB_USER}-develop
cd ${HOME}/cd-demo-manifest/handson/pipelines/prep
oc apply -f sonarqube-install.yaml
sleep 10;


# Add image-puller role on staging/production project
echo "### Add image-puller role on staging/production project ###"
oc policy add-role-to-user system:image-puller system:serviceaccount:${GITLAB_USER}-staging:default —namespace=${GITLAB_USER}-develop
oc policy add-role-to-user system:image-puller system:serviceaccount:${GITLAB_USER}-production:default —namespace=${GITLAB_USER}-develop

# Clone Sample App repositories
echo "### Clone Sample App repositories ###"
cd $HOME
git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/${GITLAB_USER}/cicd-sample-app.git
sleep 10;

# Create CI pipeline
echo "### Create CI pipeline ###"
cd ./cd-demo-manifest/
oc project ${GITLAB_USER}-develop
cat ./handson/pipelines/prep/gitlab-auth.yaml  | envsubst | oc create -f -
oc secret link pipeline gitlab-token
sleep 10;

cat ./handson/pipelines/prep/tekton-pvc.yaml | envsubst | oc create -f -
sleep 10;
oc create -f ./handson/pipelines/tasks/
sleep 10;

cat ./handson/pipelines/handson-pipeline.yaml | envsubst | oc create -f -
sleep 10;

# Run CI pipeline
cat ./handson/pipelines/handson-pipelinerun.yaml | envsubst | oc create -f -
sleep 10;

# Create CI pipeline Trigger
echo "### Create Pipeline Trigger ###"

# Create TriggerTemplate
cat ./handson/pipelines/trigger/handson-template.yaml |  envsubst | oc apply -f -
sleep 10;

# Create TriggerBinding
cat ./handson/pipelines/trigger/handson-binding.yaml |  envsubst | oc apply -f -
sleep 10;

# Create SECRET_TOKEN (SECRET_TOKEN = GITLAB_USER)
oc create secret generic gitlab-webhook --from-literal=secretkey=${GITLAB_USER}
sleep 10;

# Create ServiceAccount
cat ./handson/pipelines/trigger/sample-sa.yaml |  envsubst | oc apply -f -
sleep 10;

# Create EventListener
cat ./handson/pipelines/trigger/handson-listener.yaml |  envsubst | oc apply -f -
sleep 10;

# Create Route
oc expose service el-handson-listener

# ArgoCD Gitlab Gitlab repository registration authorization granted
oc patch argocd openshift-gitops -n openshift-gitops --type='json' -p='[{"op": "replace", "path": "/spec/rbac/policy", "value":"g, system:authenticated, role:admin\n"}]'
