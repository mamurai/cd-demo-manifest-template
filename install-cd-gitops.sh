#!/bin/bash

if [ "$(oc whoami)" != "system:admin" -a "$(oc whoami)" != "opentlc-mgr" ]; then
  echo "Login as admin or opentlc-mgr first."
  exit
fi

echo -n "Input GITLAB_USER (current: ${GITLAB_USER}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_USER=${REPLY}
fi

echo -n "Input GITLAB_USER_EMAIL (current: ${GITLAB_USER_EMAIL}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_USER_EMAIL=${REPLY}
fi

echo -n "Input GITLAB_TOKEN (current: ${GITLAB_TOKEN}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_TOKEN=${REPLY}
fi

echo GITLAB_USER:       ${GITLAB_USER}
echo GITLAB_USER_EMAIL: ${GITLAB_USER_EMAIL}
echo GITLAB_TOKEN:      ${GITLAB_TOKEN}

echo -n "Input variable is correct?  (yes or no) "
read
if [ "${REPLY}" != "yes" ]; then
  exit
fi

# Update Local Repository
git pull https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/${GITLAB_USER}/cd-demo-manifest.git
sleep 10;

# Replace <GITLAB_~> string to your own variables
cd ${HOME}/cd-demo-manifest
for i in `find ./handson/deploy -type f -name "*.yaml"`; do  cat $i | envsubst > $i.new; mv -f $i.new $i; done

# Push to remote cd-manifest repository
git config --global user.name ${GITLAB_USER}
git config --global user.email ${GITLAB_USER_EMAIL}
git commit -a -m "Replaced GITLAB_ info"
git push
sleep 10;

# Create Staging / Production brunch
git branch staging
git push -u origin staging
sleep 10;

git branch production
git push -u origin production
sleep 10;

# ArgoCD Develop Setup
echo "### ArgoCD Develop Setup ###"
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${GITLAB_USER}-develop

cd ${HOME}/cd-demo-manifest
cat ./handson/argocd/dev-project.yaml | envsubst | oc create -f -
sleep 10;

cat ./handson/argocd/dev-app.yaml | envsubst | oc create -f -
sleep 10;

# ArgoCD Staging Setup
echo "### ArgoCD Staging Setup ###"
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${GITLAB_USER}-staging
cat ./handson/argocd/stg-project.yaml | envsubst | oc create -f -
sleep 10;

cat ./handson/argocd/stg-app.yaml | envsubst | oc create -f -
sleep 10;

# ArgoCD Production Setup
echo "### ArgoCD Production Setup ###"
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${GITLAB_USER}-production
cat ./handson/argocd/prod-project.yaml | envsubst | oc create -f -
sleep 10;

cat ./handson/argocd/prod-app.yaml | envsubst | oc create -f -
sleep 10;
