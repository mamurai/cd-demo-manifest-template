# CI/CD デモ環境構築手順

Container Stater Kit で実施するハンズオンの手順を元に、OpenShiftのCICDデモ環境を簡単に作成するための手順をしめします。　

本手順では demo.redhat.com で OpenShiftを払い出した後、以下の1.2のスクリプトを実行してでも環境を構築します。なお、スクリプト実行前に、GitLabの準備、GUIやターミナルからコマンド実行が必要な作業がありますので、以下4の手順を参照して環境を構築してください。

| 項番 | スクリプト |説明 |
|----|----|----|
| 1 | install-ci-pipeline.sh | CIパイプライン作成スクリプト |
| 2 | install-cd-gitops.sh | CD (ArgoCD設定)作成スクリプト |
| 3 | delete-all-objects.sh | 1,2 で作成したオブジェクト削除用スクリプト |
| 4 | handson/text/CreateDemoEnv.md | Demo環境構築手順書

---
## 1-1 デモで利用するOpenShift環境の構築 ##
[Red Hat Demo Platform](https://demo.redhat.com) にアクセスしてOpenShiftの環境を払い出します。
今回のデモでは、カタログにある、[Red Hat OpenShift Container Platform 4 Demo](https://demo.redhat.com/catalog?search=openshift+demo&item=babylon-catalog-prod%2Fsandboxes-gpte.ocp4-demo.prod) の環境を利用しています。

* OpenShift カタログから Red Hat OpenShift Container Platform 4 Demo を選択し、**Order** をクリック。

![Demo Redhat 01](../img/demo-redhat-01.PNG)

* Activity を選択
* Purpose を選択
* チェックボックスをクリック
* Order ボタンをクリック

数十分後に払い出した環境に接続するメールが届きます。

![Demo Redhat 02](../img/demo-redhat-02.PNG)

OpenShift環境への接続情報については、[Demo Platform > Services ](https://demo.redhat.com/services/)からも確認できます。

![Demo Redhat 03](../img/demo-redhat-03.PNG)

## 1-1. アプリケーションとマニフェストのリポジトリのFork
初めに GitLab.com にサインインし、ひな形のリポジトリを Fork します。  
ひな形のリポジトリの URL は、それぞれ次の通りです。

* アプリケーションリポジトリ  
https://gitlab.com/mamurai/cicd-sample-app

* マニフェスト + インストールスクリプト リポジトリ  
https://gitlab.com/mamurai/cd-demo-manifest-template

ひな形のリポジトリの画面で右側にある **"Fork"** ボタンをクリックします。

![Gitlab Fork 01](../img/gitlab-fork-01.PNG)

すると下図のような画面になります。

**cd-demo-manifest-template** を Fork する場合、次の3点を設定します。その他の項目はそのままで構いません。
なお、cicd-sample-app は何も変更せず Fork してください。

* "Project name" を cd-demo-manifest-template から **cd-demo-manifest** に変更してしてください。
* "Project URL" の "Select a namespace" の部分をクリックし、あなた自身の Full Name を選択
* "Visibility level" で、"Private" を選択

![Gitlab Fork 02](../img/gitlab-fork-02.PNG)
最後に下部の **"Fork project"** ボタンをクリックすると、あなたのアカウントに Fork されます。

![Gitlab Fork 03](../img/gitlab-fork-03.PNG)

この操作を、アプリケーションリポジトリとマニフェストリポジトリの両方で行います。

---
## 1-2. GitLab のユーザ情報を環境変数に登録
次に、OpenShift クラスタに対し oc コマンドが実行可能な環境で、GitLab のユーザ情報を環境変数として登録します。
これら環境変数は、当ハンズオンのコマンド中や、ファイル中の文字列との置き換えなどで使用されるため、必要な手順です。

まずはあなたの GitLab アカウントの username とメールアドレスを登録します。  
**username と Full name を間違えないように注意してください。**  
GitLab の画面右上のユーザーアバター (丸形) のアイコンをクリックするとユーザーの名前が表示されますが、@(アットマーク)以下の方が username です。
![Gitlab Username 01](../img/gitlab-username-01.PNG)

```
## GitLabアカウントのusername
export GITLAB_USER=(ご自身のusername)

## GitLabアカウントので登録したメールアドレス
export GITLAB_USER_EMAIL=(ご自身のemail address)
```

次に、GitLab のアクセストークンを登録しますが、アクセストークンを作成していない人はここで取得しましょう。  
アクセストークンは、当ハンズオンで OpenShift Pipelines や OpenShift GitOps が、あなたの GitLab リポジトリを操作するために必要となります。

GitLab の画面右上のユーザーアバター (丸形) のアイコンをクリックし、**"Edit profile"** のメニューをクリックします。

![Gitlab Token 01](../img/gitlab-token-01.PNG)

次の画面の左にあるメニューから、**"Access Tokens"** をクリックし、"Personal Access Tokens" の画面を表示します。

![Gitlab Token 02](../img/gitlab-token-02.PNG)

開いた画面で次の項目を入力して、**"Create personal access token"** ボタンをクリックします。

* **Token name** : 好きな名前を入力
* **Expiration date** : アクセストークンの有効期限を自由に設定。設定しなくてもOKで、その場合はずっと有効になる。
* **Select scopes** : **"api"** にチェック

自動的にアクセストークンが作成され、上部に表示されます。  
**表示されているアクセストークンは、このページから移動すると二度と表示できなくなるため、どこかに記録しておきましょう。**  
※もしアクセストークンを失ってしまったら、もう一度同じ手順で作り直して下さい。

![Gitlab Token 03](../img/gitlab-token-03.PNG)

作成したアクセストークンを環境変数に登録します。
```
## GitLabアカウントのtoken
export GITLAB_TOKEN=(作成したtoken)
```

---
## 1-3. OpenShift クラスタへのログイン

当ハンズオンでは、あなたがクローンして入手したアプリケーションを、これまたあなたがクローンして入手したマニフェストを使って、OpenShift クラスタで稼働させます。  
OpenShift クラスタであなた用の Project を作成してアプリケーションを稼働させますが、Project を作成するために、OpenShift クラスタへログインします。

ログイン方法は、OpenShift クラスタで構成されている IdP(Identity Provider) によって多少異なります。このハンズオンテキストでは GitLab と連携する IdP を使ってログインする方法を採っていますが、他の IdP を構成している場合はその方法でログインしてください。


## 1-4. CI Demo 環境作成
### 1-4-1. Demo環境の bastion サーバへログインし、OpenShift へ CLIでログイン

demo.redhat.com から送付されてくる ssh の接続先にターミナルからログインする
```
$ ssh lab-user@bastion.zvcnl.sandbox2813.opentlc.com
```

GitLab関連の環境変数を設定する。
**項番 1.2 で実施済みの場合は本手順は飛ばしてください。**
```
## GitLabアカウントのusername
export GITLAB_USER=(ご自身のusername)

## GitLabアカウントので登録したメールアドレス
export GITLAB_USER_EMAIL=(ご自身のemail address)

## GitLabアカウント TOKEN　(APIアクセス許可 GitLabs設定済み)
export GITLAB_TOKEN=(作成したtoken)
```

OpenShift に管理者権限を持つアカウントでログインします。**(ID: opentlc-mgr Passwd: r3dh4t1!)**
```
oc login -u opentlc-mgr
```

ご自身のGitLabsのリポジトリから (マニフェスト + インストールスクリプト リポジトリ) をクローンします。
```
cd ${HOME}
git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/${GITLAB_USER}/cd-demo-manifest.git
```

CI環境作成用スクリプトを実行します。  
GITLAB 関連の環境変数を設定済みの場合、環境変数の値を確認後、**yes** を入力すれば処理が始まります。
```
cd cd-demo-manifest/
sh install-ci-pipeline.sh
```

**install-ci-pipeline.sh スクリプトの実行内容**
* Pipeline & Gitops Operator install
* プロジェクトの作成 (develop / staging / production)
* develop プロジェクトに sonarqube をデプロイ
* staging / production プロジェクトに image-puller の権限追加
* CICDで利用するサンプルアプリケーション **cicd-sample-app** の クローン
* CIパイプライン作成
* CIパイプライン トリガーの作成
* Webhookで利用する route の作成
* ArgoCD を利用できる様に 権限を付与



### 1-4-2. パイプライン実行の確認
CI環境作成スクリプトの中で PipelineRunを実行しているので、
OpenShift Web コンソールに入って、パイプラインがうまく実行されたか確認できます。

Web コンソールの左のメニューから **"Pipelines" > "Pipelines"** を選択し、画面上部の Project を選択する場所で **"{GITLAB_USER}-develop"** を選択します。  
そして、右側の画面で **"PipelineRuns"** のタブをクリックすると、"PLR" というマークが付いたエントリーが見つかります。  
これの Status が **"Succeeded"** になっていれば成功です。何か処理が動いているようであれば、終わるまで待ちます。(おおむね5分も待てばよいでしょう)

![PipelineRun Success 01](../img/pipelinerun-success-01.PNG)

エントリーの名前をクリックすると、実行されたパイプラインの中身やログを見ることができます。

![PipelineRun Success 02](../img/pipelinerun-success-02.PNG)


## 1-5. CD Demo 環境作成
### 1-5-1 ArgoCD へのログイン
OpenShift GitOps は内部で Argo CD というオープンソースのソフトウェアが稼働しています。  
Argo CD は GUI を持っており、OpenShift の OAuth でログインすることができます。ここでログインしてみましょう。

OpenShift Web コンソールにログインすると、画面上部に小さな四角が3x3で並んだアイコンが見つかります。  
ここをクリックすると、**"Cluster Argo CD"** というエントリーが見つかります。これをクリックします。

![Argo CD UI Login 01](../img/argocd-login-01.PNG)

すると、別ウィンドウで Argo CD の UI が開きます。

![Argo CD UI Login 02](../img/argocd-login-02.PNG)

この画面の右側にある、**"LOG IN VIA OPENSHIFT"** をクリックすると、OpenShift Web コンソールのログインの画面に移ります。  
ログインすると、次のようなアクセス認証の画面が表示されるので、**user:Info** にチェックを入れたまま **"Allow selected permissions"** ボタンをクリックします。

![Argo CD UI Login 03](../img/argocd-login-03.PNG)

Argo CD の GUI にログインします。現時点では OpenShift GitOps で何も設定していないので、"No applications yet" と何もない状態で表示されています。

![Argo CD UI Login 04](../img/argocd-login-04.PNG)

### 1-5-2 ArgoCDの画面よりリポジトリーを登録する

ログインしたら初めに、Argo CD に GitLab で Fork したあなたのマニフェストリポジトリを接続します。  
これは Argo CD がマニフェストを取得するために必要な作業です。

UI の左端にある歯車のアイコンをクリックすると、"Settings" 画面に移ります。そこで一番上にある **"Repositories"** をクリックします。

![Argo CD Repo 01](../img/argocd-repo-01.PNG)

下図のような画面になります。ここで **"CONNECT REPO"** をクリックします。

![Argo CD Repo 02](../img/argocd-repo-02.PNG)

すると接続するリポジトリの情報を入力する画面が表示されます。以下の項目を入力して、**"CONNECT"** ボタンをクリックします。

* Choose your connection method  
VIA HTTPS
* Type  
git
* Project  
(空白)
* Repository URL  
あなたの GitLab の cd-manifest リポジトリの URL ※末尾に ".git" を忘れないように注意
* Username/Password  
あなたの GitLab アカウントの username とパスワードをそれぞれ入力

![Argo CD Repo 03](../img/argocd-repo-03.PNG)

"CONNECTION STATUS" が "Successful" と表示されれば、接続に成功です。

![Argo CD Repo 04](../img/argocd-repo-04.PNG)
### 1-5-2 CD環境インストールスクリプトを実行する
```
cd $HOME/cd-demo-manifest
sh install-cd-gitops.sh
```
**install-cd-gitops.sh スクリプトの実行内容**
* git pull を実行しローカルを最新に変更 (**cd-demo-manifest**が CIパイプライン実行で更新済みのため)
* ./handson/deploy 以下の yaml ファイルに GITLAB 関連の環境変数を反映
* 変更内容を commit して、Gitlab に push
* staging ブランチを作成し、リモートにpush (ステージング環境更新用ブランチ)
* production ブランチを作成し、リモートにpush (本番環境更新用ブランチ)
* 開発環境の argocd プロジェクト と アプリケーションを作成
* ステージング環境の argocd プロジェクト と アプリケーションを作成
* 本番環境の argocd プロジェクト と アプリケーションを作成

### 1-5-3 CD環境インストール確認
Application 画面をひらくと、開発、ステージング、本番の３環境用のアプリケーション設定が作成されています。
![Argo CD Deploy Check 01](../img/argocd-deploy-check-01.PNG)

開発環境の ${GITLAB_USER}-app-develop をクリックし詳細を表示すると、以下のようにオブジェクトが作成されていることが確認できます。  
**APP HEALTH** 、**CURRENT SYNC STATUS** 、**LAST SYNC RESULT** が正常であることを画面から確認します。

![Argo CD Deploy Check 02](../img/argocd-deploy-check-02.PNG)

## 1-6 GitLab Webhook の設定

CLIより次のコマンドを実行し、Webhookで利用するURLを取得する
```
oc get route el-handson-listener -n ${GITLAB_USER}-develop

[出力結果(例)]
NAME                  HOST/PORT                                                                              PATH   SERVICES              PORT            TERMINATION   WILDCARD
el-handson-listener   el-handson-listener-${GITLAB_USER}.apps.cluster-zvcnl.zvcnl.sandbox2813.opentlc.com          el-handson-listener   http-listener                 None

```

https://gitlab.com/${GITLAB_USER}/cicd-sample-app へアクセスしてWebhookを設定します。

+ Settings > webhooks をクリック
* URL に CLI実行結果で取得した HOST/PORT の内容を **http://** を頭につけて入力
* Secret token に **${GITLAB_USER}** の値を入力
* Push events にチェックをいれる
* Regular expression を選択し、テキストボックスに **"main"** を入力する
* Add webhook ボタンをクリックする

![GitLab Webhook Check 01](../img/gitlab-webhook-set-01.PNG)

追加された Webhookのテストを実行します
* Test をクリック
* 選択リストから **"Push events"** を選択

![GitLab Webhook Check 02](../img/gitlab-webhook-set-02.PNG)

OpenShiftの管理コンソール上から、PipelineRun が新規で実行されていることが確認できます。

![GitLab Webhook Check 03](../img/gitlab-webhook-set-03.PNG)

---
準備作業は以上です。
