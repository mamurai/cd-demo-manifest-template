# Starter Kit : 運用編3日目 CD ハンズオン

Container Stater Kit で実施するハンズオンの手順を紹介します。  
当ハンズオンによって、OpenShift GitOps を使ったアプリケーションの CD(継続的デリバリー) を体験することができます。

0 章は、当ハンズオンを実施するためにクラスターをセットアップする手順です。OpenShift クラスタの管理者権限が必要な手順が含まれていますので、ハンズオンまでにクラスタ管理者の方が行って下さい。

ハンズオンを受講する方は、1 章から実施して下さい。

---
## 【OpenShift CI/CD デモ環境作成】
## [Starter Kit の CD コンテンツを OpenShiftへデプロイ](./text/CreateDemoEnv.md)

以下の手順にある、Container Starter Kit のハンズオンで作り上げる CDの環境作成を、極力スクリプト化。OpenShift Pipeline と GitOps のデモ環境を簡単に入手することを目的としています。

---
## 【クラスタ管理者向け】
## [0 章: ハンズオンのためのクラスターセットアップ](./text/Chapter-0.md)

---
## 【ハンズオン受講者向け】
## [1 章: CD でデプロイするアプリケーション環境の事前準備](./text/Chapter-1.md)
## [2 章: OpenShift GitOps を使ったアプリケーションのデプロイ](./text/Chapter-2.md)
## [3 章: 開発環境のアプリケーション更新をステージング環境～本番環境に反映](./text/Chapter-3.md)
