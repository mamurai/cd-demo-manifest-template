#!/bin/#!/usr/bin/env bash

if [ "$(oc whoami)" != "system:admin" -a "$(oc whoami)" != "opentlc-mgr" ]; then
  echo "Login as admin or opentlc-mgr first."
  exit
fi

echo -n "Input GITLAB_USER (current: ${GITLAB_USER}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_USER=${REPLY}
fi

echo -n "Input GITLAB_USER_EMAIL (current: ${GITLAB_USER_EMAIL}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_USER_EMAIL=${REPLY}
fi

echo -n "Input GITLAB_TOKEN (current: ${GITLAB_TOKEN}): "
read
if [ "${REPLY}" != "" ]; then
  export GITLAB_TOKEN=${REPLY}
fi

echo GITLAB_USER:       ${GITLAB_USER}
echo GITLAB_USER_EMAIL: ${GITLAB_USER_EMAIL}
echo GITLAB_TOKEN:      ${GITLAB_TOKEN}

echo -n "Input variable is correct?  (yes or no) "
read
if [ "${REPLY}" != "yes" ]; then
  exit
fi


cd  ${HOME}/cd-demo-manifest

# ArgoCD Delete Script
cat ./handson/argocd/dev-project.yaml | envsubst | oc delete -f -
cat ./handson/argocd/dev-app.yaml | envsubst | oc delete -f -
cat ./handson/argocd/stg-project.yaml | envsubst | oc delete -f -
cat ./handson/argocd/stg-app.yaml | envsubst | oc delete -f -
cat ./handson/argocd/prod-project.yaml | envsubst | oc delete -f -
cat ./handson/argocd/prod-app.yaml | envsubst | oc delete -f -

# Project Delete Script
oc delete project ${GITLAB_USER}-develop
oc delete project ${GITLAB_USER}-staging
oc delete project ${GITLAB_USER}-production


# Operator delete
oc delete -f  ./handson/pipelines/prep/openshift-pipeline-install.yaml
oc delete -f  ./handson/pipelines/prep/openshift-gitops-install.yaml
